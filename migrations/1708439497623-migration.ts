import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1708439497623 implements MigrationInterface {
    name = 'Migration1708439497623'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "email" character varying, "phone_number" character varying(12), "roles" json NOT NULL DEFAULT '["ROLE_USER"]', "password" character varying NOT NULL, "deleted" TIMESTAMP, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "upload" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "filepath" character varying NOT NULL, CONSTRAINT "PK_1fe8db121b3de4ddfa677fc51f3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying(255) NOT NULL, "title" character varying(255) NOT NULL, "type" bigint NOT NULL DEFAULT '1', "status" boolean NOT NULL DEFAULT true, "image_id" uuid, CONSTRAINT "REL_dc252738f70366595ac88f5a98" UNIQUE ("image_id"), CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "producer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "status" boolean NOT NULL DEFAULT true, CONSTRAINT "PK_4cfe496c2c70e4c9b9f444525a6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "image_product_pivot" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "sort" integer NOT NULL DEFAULT '0', "montage" integer, "image_id" uuid NOT NULL, "product_id" uuid NOT NULL, CONSTRAINT "REL_9e040c9a696d8fc7681e70af74" UNIQUE ("image_id"), CONSTRAINT "PK_8803e4c13e5daf44c99fb8b18b8" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "featureGroup" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "sort" bigint NOT NULL DEFAULT '0', CONSTRAINT "PK_40a23f4ae96f4fa9e1ebbf5dc84" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "feature" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "key" character varying NOT NULL, "type" bigint NOT NULL DEFAULT '1', "sort" integer NOT NULL DEFAULT '0', "isSearchInFilter" boolean NOT NULL DEFAULT false, "status" boolean NOT NULL DEFAULT true, "group_id" uuid, CONSTRAINT "UQ_d4c0dcbc7a2c14e22d840761db9" UNIQUE ("key"), CONSTRAINT "PK_03930932f909ca4be8e33d16a2d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "feature_product" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "values" jsonb NOT NULL DEFAULT '[]', "sort" integer NOT NULL DEFAULT '0', "status" boolean NOT NULL DEFAULT true, "product_id" uuid NOT NULL, "feature_id" uuid NOT NULL, CONSTRAINT "PK_1e42eaaea95e7a0340dd273d15d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "product" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying(255) NOT NULL, "key" character varying(400) NOT NULL, "infoBlock" jsonb, "mainInfo" jsonb, "featuresBlock" jsonb NOT NULL DEFAULT '[]', "services" jsonb NOT NULL DEFAULT '[]', "description" text NOT NULL, "advantages" jsonb NOT NULL DEFAULT '[]', "mainProperties" jsonb NOT NULL DEFAULT '[]', "price" integer NOT NULL, "tags" jsonb NOT NULL DEFAULT '[]', "videoReview" jsonb, "montageType" integer, "status" boolean NOT NULL DEFAULT true, "main_image_id" uuid NOT NULL, "down_image_id" uuid NOT NULL, "category_id" uuid NOT NULL, "producer_id" uuid NOT NULL, CONSTRAINT "UQ_120ea9ee74ef0e0774629d448e4" UNIQUE ("key"), CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "product-docs" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "description" text NOT NULL, "product_id" uuid NOT NULL, CONSTRAINT "REL_c1715963c26fe9096111844bf1" UNIQUE ("product_id"), CONSTRAINT "PK_43be8dd54e53150f1f50a469a3b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "doc" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL DEFAULT 'документ', "upload_id" uuid, "product_doc_id" uuid, CONSTRAINT "PK_10d9859fc620db615c8aa74e324" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "product_buy_with_products_product" ("productId_1" uuid NOT NULL, "productId_2" uuid NOT NULL, CONSTRAINT "PK_2cf7a29660ebd57199505804fac" PRIMARY KEY ("productId_1", "productId_2"))`);
        await queryRunner.query(`CREATE INDEX "IDX_657b40012dbfb07083c1c9768d" ON "product_buy_with_products_product" ("productId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_d5a0b4c18c8d5a9d9213fb6a71" ON "product_buy_with_products_product" ("productId_2") `);
        await queryRunner.query(`ALTER TABLE "category" ADD CONSTRAINT "FK_dc252738f70366595ac88f5a98f" FOREIGN KEY ("image_id") REFERENCES "upload"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "image_product_pivot" ADD CONSTRAINT "FK_9e040c9a696d8fc7681e70af745" FOREIGN KEY ("image_id") REFERENCES "upload"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "image_product_pivot" ADD CONSTRAINT "FK_cfdf12f84d9ccb2acb33b089b95" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "feature" ADD CONSTRAINT "FK_b2868a3c413f050e6a98b580efc" FOREIGN KEY ("group_id") REFERENCES "featureGroup"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "feature_product" ADD CONSTRAINT "FK_61640e7bfc492e5ad7156885b10" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "feature_product" ADD CONSTRAINT "FK_ed3bdcec6e6f4842b278dfca519" FOREIGN KEY ("feature_id") REFERENCES "feature"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "FK_4fc42eba35343ea2e2068318698" FOREIGN KEY ("main_image_id") REFERENCES "upload"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "FK_dabe098d5e1a6dd46f1d448cd6c" FOREIGN KEY ("down_image_id") REFERENCES "upload"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "FK_0dce9bc93c2d2c399982d04bef1" FOREIGN KEY ("category_id") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "FK_d478a408853788d463d987149eb" FOREIGN KEY ("producer_id") REFERENCES "producer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product-docs" ADD CONSTRAINT "FK_c1715963c26fe9096111844bf13" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "doc" ADD CONSTRAINT "FK_e8a4434243d2fc0533cfb88107c" FOREIGN KEY ("upload_id") REFERENCES "upload"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "doc" ADD CONSTRAINT "FK_5f37c9264756e6d6d40018bf3f6" FOREIGN KEY ("product_doc_id") REFERENCES "product-docs"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product_buy_with_products_product" ADD CONSTRAINT "FK_657b40012dbfb07083c1c9768da" FOREIGN KEY ("productId_1") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "product_buy_with_products_product" ADD CONSTRAINT "FK_d5a0b4c18c8d5a9d9213fb6a71c" FOREIGN KEY ("productId_2") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product_buy_with_products_product" DROP CONSTRAINT "FK_d5a0b4c18c8d5a9d9213fb6a71c"`);
        await queryRunner.query(`ALTER TABLE "product_buy_with_products_product" DROP CONSTRAINT "FK_657b40012dbfb07083c1c9768da"`);
        await queryRunner.query(`ALTER TABLE "doc" DROP CONSTRAINT "FK_5f37c9264756e6d6d40018bf3f6"`);
        await queryRunner.query(`ALTER TABLE "doc" DROP CONSTRAINT "FK_e8a4434243d2fc0533cfb88107c"`);
        await queryRunner.query(`ALTER TABLE "product-docs" DROP CONSTRAINT "FK_c1715963c26fe9096111844bf13"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "FK_d478a408853788d463d987149eb"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "FK_0dce9bc93c2d2c399982d04bef1"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "FK_dabe098d5e1a6dd46f1d448cd6c"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "FK_4fc42eba35343ea2e2068318698"`);
        await queryRunner.query(`ALTER TABLE "feature_product" DROP CONSTRAINT "FK_ed3bdcec6e6f4842b278dfca519"`);
        await queryRunner.query(`ALTER TABLE "feature_product" DROP CONSTRAINT "FK_61640e7bfc492e5ad7156885b10"`);
        await queryRunner.query(`ALTER TABLE "feature" DROP CONSTRAINT "FK_b2868a3c413f050e6a98b580efc"`);
        await queryRunner.query(`ALTER TABLE "image_product_pivot" DROP CONSTRAINT "FK_cfdf12f84d9ccb2acb33b089b95"`);
        await queryRunner.query(`ALTER TABLE "image_product_pivot" DROP CONSTRAINT "FK_9e040c9a696d8fc7681e70af745"`);
        await queryRunner.query(`ALTER TABLE "category" DROP CONSTRAINT "FK_dc252738f70366595ac88f5a98f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d5a0b4c18c8d5a9d9213fb6a71"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_657b40012dbfb07083c1c9768d"`);
        await queryRunner.query(`DROP TABLE "product_buy_with_products_product"`);
        await queryRunner.query(`DROP TABLE "doc"`);
        await queryRunner.query(`DROP TABLE "product-docs"`);
        await queryRunner.query(`DROP TABLE "product"`);
        await queryRunner.query(`DROP TABLE "feature_product"`);
        await queryRunner.query(`DROP TABLE "feature"`);
        await queryRunner.query(`DROP TABLE "featureGroup"`);
        await queryRunner.query(`DROP TABLE "image_product_pivot"`);
        await queryRunner.query(`DROP TABLE "producer"`);
        await queryRunner.query(`DROP TABLE "category"`);
        await queryRunner.query(`DROP TABLE "upload"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
