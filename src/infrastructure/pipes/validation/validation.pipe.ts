import {
  ArgumentMetadata,
  Injectable,
  ValidationPipe as BaseValidationPipe,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { ValidationException } from '@/infrastructure/pipes/validation/validation.exception';
import { ValidatorOptions } from '@nestjs/common/interfaces/external/validator-options.interface';
import { ValidationError } from '@nestjs/common/interfaces/external/validation-error.interface';

@Injectable()
export class ValidationPipe extends BaseValidationPipe {
  protected async validate(
    object: object,
    validatorOptions?: ValidatorOptions,
  ) {
    const errors = await super.validate(object, validatorOptions);
    if (errors.length) {
      const messages: object = {};

      errors.forEach((err) => {
        if (typeof err?.property !== 'string' || !err?.constraints) {
          return;
        }

        // @ts-ignore
        messages[err.property] = Object.values(err.constraints).join(',');
      });

      throw new ValidationException(messages);
    }

    return errors;
  }
}
