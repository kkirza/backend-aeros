import * as dotenv from 'dotenv';
import * as dotenvExpand from 'dotenv-expand';
import { DataSource } from 'typeorm';
import * as process from 'process';
import { User } from '@/domain/user/user.entity';
import { Category } from '@/domain/category/category.entity';
import { Doc } from '@/domain/doc/doc.entity';
import { Feature } from '@/domain/feature/feature.entity';
import { FeatureGroup } from '@/domain/featureGroup/featureGroup.entity';
import { FeatureProduct } from '@/domain/featureProduct/featureProduct.entity';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { Producer } from '@/domain/producer/producer.entity';
import { Product } from '@/domain/product/product.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { Upload } from '@/domain/upload/upload.entity';

dotenvExpand.expand(dotenv.config({ path: '.env' }));

const port: number = Number(process.env.DB_PORT) || 5432;

export default new DataSource({
  type: 'postgres',
  host: process.env.DB_HOST,
  port: port,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: true,
  entities: [
    User,
    Category,
    Doc,
    Feature,
    FeatureGroup,
    FeatureProduct,
    ImageProductPivot,
    Producer,
    Product,
    ProductDocs,
    Upload,
  ],
  migrationsTableName: 'migrations',
  migrationsRun: true,
  migrationsTransactionMode: 'each',
  migrations: ['dist/migrations/*.js'],
});
