export interface CommandInterface {
  execute(): Promise<void>;
}
