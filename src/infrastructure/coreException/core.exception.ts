import { HttpException, HttpStatus } from '@nestjs/common';

export class CoreException extends HttpException {
  public constructor(
    message: string = 'Непредвиденная ошибка',
    status: number = 400,
    error: any[] = [],
  ) {
    super({ status, message, error }, HttpStatus.BAD_REQUEST);
  }
}
