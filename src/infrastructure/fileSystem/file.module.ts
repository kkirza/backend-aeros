import { Module } from '@nestjs/common';
import { FileService } from '@/infrastructure/fileSystem/file.service';

@Module({
  providers: [FileService],
  exports: [FileService],
})
export class FileModule {}
