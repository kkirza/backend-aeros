export class FileItem {
  public fieldname: string;
  public originalname: string;
  public encoding: string;
  public mimetype: string;
  public size: number;
  public destination: string;
  public filename: string;
  public path: string;
  public buffer: Buffer;
}
