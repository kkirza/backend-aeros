import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import { promises as asyncFs } from 'fs';
// @ts-ignore
import * as uuid from 'uuid';
import { FileItem } from '@/infrastructure/fileSystem/fileItem';

@Injectable()
export class FileService {
  public async createFile(
    file: Express.Multer.File,
    dirName?: string,
  ): Promise<string> {
    try {
      const originalName: string = file.originalname;
      const arrayName: string[] = originalName.split('.');
      const ext: string = arrayName[arrayName.length - 1];
      const fileName: string = uuid.v4() + `.${ext}`;
      let filePath: string = path.resolve(
        __dirname,
        '..',
        '..',
        '..',
        '..',
        'static',
      );
      if (dirName) {
        filePath += '/dirname';
      }

      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }
      fs.writeFileSync(path.join(filePath, fileName), file.buffer);

      return fileName;
    } catch (e) {
      throw new HttpException(
        'Непредвиденная ошибка при записи файла',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public async DeleteFileByPath(filePath: string): Promise<void> {
    await asyncFs.unlink(filePath);
  }
}
