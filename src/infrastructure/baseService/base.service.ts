import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource, FindManyOptions } from 'typeorm';
import { ObjectType } from 'typeorm/common/ObjectType';
import { HttpException } from '@nestjs/common';
import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';

export class BaseService<Entity extends BaseEntity> {
  public constructor(
    @InjectDataSource()
    protected readonly dataSource: DataSource,
    protected readonly entityType: () => ObjectType<Entity>,
  ) {}

  public async findOneById(id: string): Promise<Entity> {
    // @ts-ignore
    const entity = await this.dataSource.manager.findOneBy(this.entityType(), {
      id,
    });

    if (!entity) {
      throw new HttpException('Not found by id ' + id, 404);
    }

    return entity;
  }

  public async findAll<Entity extends BaseEntity>(
    options?: FindManyOptions<Entity> | undefined,
  ): Promise<Entity[]> {
    return await this.dataSource.manager.find<Entity>(
      this.entityType(),
      options,
    );
  }

  public async deleteOneById<Entity extends BaseEntity>(
    id: string,
  ): Promise<void> {
    const entity = await this.dataSource.manager.findOneBy<Entity>(
      this.entityType(),
      // @ts-ignore не резолвит, но реализация работает.
      { id },
    );
    if (!entity) {
      return;
    }

    await this.dataSource.manager.delete(this.entityType(), { id });
  }
}
