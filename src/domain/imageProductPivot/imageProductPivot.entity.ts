import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Upload } from '@/domain/upload/upload.entity';
import { Product } from '@/domain/product/product.entity';

@Entity('image_product_pivot')
export class ImageProductPivot extends BaseEntity {
  @OneToOne(() => Upload, { nullable: false, eager: true })
  @JoinColumn([{ name: 'image_id', referencedColumnName: 'id' }])
  public image: Upload;

  @ManyToOne(() => Product, { nullable: false, eager: false })
  @JoinColumn([{ name: 'product_id', referencedColumnName: 'id' }])
  public product: Product;

  @ApiProperty({ example: 0, description: 'Сортировка вывода на фронте' })
  @Column('int', { nullable: false, default: 0 })
  public sort: number = 0;

  @ApiProperty({ example: 0, description: 'На какой картинке монтаж' })
  @Column('int', { nullable: true, default: null })
  public montage: number | null = null;
}
