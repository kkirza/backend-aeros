import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { Upload } from '@/domain/upload/upload.entity';
import { Product } from '@/domain/product/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ImageProductPivot, Upload, Product])],
  exports: [TypeOrmModule],
})
export class ImageProductPivotModule {}
