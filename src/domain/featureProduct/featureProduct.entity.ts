import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Product } from '@/domain/product/product.entity';
import { Feature } from '@/domain/feature/feature.entity';

@Entity('feature_product')
export class FeatureProduct extends BaseEntity {
  @ManyToOne(() => Product, { nullable: false, eager: false })
  @JoinColumn([{ name: 'product_id', referencedColumnName: 'id' }])
  public product: Product;

  @ManyToOne(() => Feature, { nullable: false, eager: true })
  @JoinColumn([{ name: 'feature_id', referencedColumnName: 'id' }])
  public feature: Feature;

  @ApiProperty({
    example: ['test', '2'],
    description: 'Список значений характеристики',
  })
  @Column('jsonb', { nullable: false, default: [] })
  public values: any[] = [];

  @ApiProperty({ example: 0, description: 'Список на выводе фронта' })
  @Column('int', { nullable: false, default: 0 })
  public sort: number = 0;

  @ApiProperty({ example: true })
  @Column('boolean', { nullable: false, default: true })
  public status: boolean = true;
}
