import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeatureProduct } from '@/domain/featureProduct/featureProduct.entity';
import { Feature } from '@/domain/feature/feature.entity';
import { Product } from '@/domain/product/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([FeatureProduct, Feature, Product])],
  exports: [TypeOrmModule],
})
export class FeatureProductModule {}
