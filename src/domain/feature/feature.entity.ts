import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { FeatureGroup } from '@/domain/featureGroup/featureGroup.entity';

@Entity('feature')
export class Feature extends BaseEntity {
  public static TYPE_STRING: number = 1;
  public static TYPE_ENUM: number = 2;
  public static TYPE_BOOL: number = 3;
  public static TYPE_INT: number = 4;
  public static TYPE_FLOAT: number = 5;
  public static AVAILABLE_TYPES: number[] = [
    Feature.TYPE_BOOL,
    Feature.TYPE_STRING,
    Feature.TYPE_ENUM,
    Feature.TYPE_INT,
    Feature.TYPE_FLOAT,
  ];

  @ApiProperty({ example: 'Масса', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false })
  public title: string;

  @ApiProperty({
    example: 'massa',
    description: 'Пустым быть не может, должно быть уникальным.',
  })
  @Column('varchar', { nullable: false, unique: true })
  public key: string;

  @ApiProperty({
    example: Feature.TYPE_STRING,
    description: 'Тип характеристики',
  })
  @Column('int8', { nullable: false, default: Feature.TYPE_STRING })
  public type: number = Feature.TYPE_STRING;

  @ApiProperty({ example: 0, description: 'Сортировка на фронте' })
  @Column('int', { nullable: false, default: 0 })
  public sort: number = 0;

  @ApiProperty({
    example: false,
    description: 'Показывать ли в списке фильтров',
  })
  @Column('boolean', { nullable: false, default: false })
  public isSearchInFilter: boolean = false;

  @ManyToOne(() => FeatureGroup, { eager: true })
  @JoinColumn([{ name: 'group_id', referencedColumnName: 'id' }])
  public group: FeatureGroup;

  @ApiProperty({ example: true })
  @Column('boolean', { nullable: false, default: true })
  public status: boolean = true;
}
