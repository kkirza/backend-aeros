import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Feature } from '@/domain/feature/feature.entity';
import { FeatureGroup } from '@/domain/featureGroup/featureGroup.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Feature, FeatureGroup])],
  exports: [TypeOrmModule],
})
export class FeatureModule {}
