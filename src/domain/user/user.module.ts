import { Module } from '@nestjs/common';
import { UsersController } from '@/application/bff/user/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@/domain/user/user.entity';

@Module({
  controllers: [UsersController],
  imports: [TypeOrmModule.forFeature([User])],
  exports: [TypeOrmModule],
})
export class UserModule {}
