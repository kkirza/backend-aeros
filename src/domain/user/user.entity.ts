import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Column, DeleteDateColumn, Entity } from 'typeorm';

@Entity('user')
export class User extends BaseEntity {
  @ApiProperty({ example: 'Иванов Иван', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false })
  public name: string;

  @ApiProperty({ example: 'ivanov@ya.ru' })
  @Column('varchar', { nullable: true })
  public email?: string;

  @Column({ type: 'varchar', length: 12, name: 'phone_number', nullable: true })
  public phoneNumber?: string;

  @Column({ type: 'json', default: ['ROLE_USER'] })
  public roles: string[];

  @ApiProperty({ example: 'm.xcfkcvmz4dffkjjhjsdaeqewSdzx' })
  @Exclude()
  @Column('varchar', { nullable: false })
  public password: string;

  @ApiProperty({ example: '2023-06-11T13:35:39.000Z' })
  @DeleteDateColumn()
  public deleted: Date;
}
