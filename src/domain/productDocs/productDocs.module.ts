import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { Product } from '@/domain/product/product.entity';
import { Doc } from '@/domain/doc/doc.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProductDocs, Product, Doc])],
  exports: [TypeOrmModule],
})
export class ProductDocsModule {}
