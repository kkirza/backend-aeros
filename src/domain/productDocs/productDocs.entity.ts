import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Product } from '@/domain/product/product.entity';
import { Doc } from '@/domain/doc/doc.entity';

@Entity('product-docs')
export class ProductDocs extends BaseEntity {
  @OneToOne(() => Product, (product: Product) => product.productDoc, {
    nullable: false,
    eager: true,
  })
  @JoinColumn([{ name: 'product_id', referencedColumnName: 'id' }])
  public product: Product;

  @OneToMany(() => Doc, (doc: Doc) => doc.productDoc)
  public docFiles: Doc[];

  @ApiProperty({
    example: 'Длинное описание документации',
    description: 'Описание документации',
  })
  @Column('text', { nullable: false })
  public description: string;
}
