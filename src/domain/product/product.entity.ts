import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Category } from '@/domain/category/category.entity';
import { Producer } from '@/domain/producer/producer.entity';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { FeatureProduct } from '@/domain/featureProduct/featureProduct.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { Service } from '@/domain/product/dto/service.dto';
import { Upload } from '@/domain/upload/upload.entity';
import { InfoBlock } from '@/domain/product/dto/infoBlock.dto';
import { MainInfo } from '@/domain/product/dto/mainInfo/mainInfo.dto';
import { FeatureBlock } from '@/domain/product/dto/featureBlock.dto';
import { VideoReview } from '@/domain/product/dto/videoReview.dto';

@Entity('product')
export class Product extends BaseEntity {
  public static MONTAGE_TYPE_BRIZER = 1;
  public static MONTAGE_TYPE_CONDITIONER = 2;

  @ApiProperty({ example: 'Tion', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false, length: 255 })
  public name: string;

  @ApiProperty({
    example: 'Tion-4S',
    description: 'Пустым быть не может, уникальное поле',
  })
  @Column('varchar', { nullable: false, length: 400, unique: true })
  public key: string;

  @ManyToOne(() => Upload, undefined, { eager: true, nullable: false })
  @JoinColumn({ name: 'main_image_id', referencedColumnName: 'id' })
  public mainImage: Upload;

  @ManyToOne(() => Upload, undefined, { eager: true, nullable: false })
  @JoinColumn({ name: 'down_image_id', referencedColumnName: 'id' })
  public downImage: Upload;

  @ApiProperty({ type: InfoBlock })
  @Column('jsonb', { nullable: true, default: null })
  public infoBlock?: InfoBlock | null;

  @ApiProperty({ type: MainInfo })
  @Column('jsonb', { nullable: true, default: null })
  public mainInfo?: MainInfo | null;

  @ApiProperty({ type: [FeatureBlock] })
  @Column('jsonb', { nullable: false, default: [] })
  public featuresBlock?: FeatureBlock[] | null;

  @ApiProperty({ type: [Product] })
  @ManyToMany(() => Product, { eager: false })
  @JoinTable()
  public buyWithProducts: Product[];

  @ApiProperty({ type: [Service] })
  @Column('jsonb', { default: [], nullable: false })
  public services?: Service[] | null;

  @ApiProperty({
    example: 'Очень длинное описание',
    description: 'Блок описания товара',
  })
  @Column('text', { nullable: false })
  public description: string;

  @ApiProperty({
    example: ['Приемущество', 'воздух'],
    description: 'Приемущества товара',
  })
  @Column('jsonb', { nullable: false, default: [] })
  public advantages: string[] = [];

  @ApiProperty({
    example: ['Работает через приложение', 'Защита от вирусов и бактерий'],
    description: 'В описании основные свойства',
  })
  @Column('jsonb', { nullable: false, default: [] })
  public mainProperties: string[] = [];

  @ApiProperty({
    example: 100000,
    description: 'Цена товара, в integer, делить на 1000',
  })
  @Column('int', { nullable: false })
  public price: number; // refactor from table city_product.price

  @ApiProperty({
    example: ['мощный', 'до 30м'],
    description: 'тэги под товаром',
  })
  @Column('jsonb', { nullable: false, default: [] })
  public tags: string[] = [];

  @ManyToOne(() => Category, undefined, { eager: true, nullable: false })
  @JoinColumn({ name: 'category_id', referencedColumnName: 'id' })
  public category: Category;

  @ManyToOne(() => Producer, undefined, { eager: true, nullable: false })
  @JoinColumn({ name: 'producer_id', referencedColumnName: 'id' })
  public producer: Producer;

  @OneToMany(
    () => ImageProductPivot,
    (pivot: ImageProductPivot) => pivot.product,
    { eager: true },
  )
  public image: ImageProductPivot[];

  @OneToMany(() => FeatureProduct, (pivot: FeatureProduct) => pivot.product, {
    eager: true,
  })
  public featurePivot: FeatureProduct[];

  @ApiProperty({ type: VideoReview })
  @Column('jsonb', { nullable: true, default: null })
  public videoReview?: VideoReview | null;

  @OneToOne(
    () => ProductDocs,
    (productDoc: ProductDocs) => productDoc.product,
    { nullable: true },
  )
  public productDoc?: ProductDocs | null = null;

  @ApiProperty({ example: Product.MONTAGE_TYPE_BRIZER })
  @Column('int', { nullable: true, default: null })
  public montageType?: number | null;

  @ApiProperty({ example: true })
  @Column('boolean', { nullable: false, default: true })
  public status: boolean = true;
}
