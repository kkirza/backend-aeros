import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from '@/domain/product/product.entity';
import { Category } from '@/domain/category/category.entity';
import { Producer } from '@/domain/producer/producer.entity';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { FeatureProduct } from '@/domain/featureProduct/featureProduct.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { ProductBackofficeController } from '@/application/backoffice/product/product.controller';
import { ProductService } from '@/services/product/product.service';
import { Upload } from '@/domain/upload/upload.entity';
import { Doc } from '@/domain/doc/doc.entity';

@Module({
  controllers: [ProductBackofficeController],
  imports: [
    TypeOrmModule.forFeature([
      Product,
      Category,
      Producer,
      ImageProductPivot,
      FeatureProduct,
      ProductDocs,
      Upload,
      Doc,
    ]),
  ],
  exports: [TypeOrmModule],
  providers: [ProductService],
})
export class ProductModule {}
