import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class FeatureBlock {
  @ApiProperty({ example: 'string' })
  @IsNotEmpty()
  @IsString()
  public headline: string;

  @ApiProperty({ example: 'string' })
  @IsNotEmpty()
  @IsString()
  public text: string;
}
