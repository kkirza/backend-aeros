import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class InfoBlock {
  @ApiProperty({ example: 'фильтрация' })
  @IsNotEmpty()
  @IsString()
  public headline: string;

  @ApiProperty({ example: 'G4 + E11 + угольный фильтр' })
  @IsNotEmpty()
  @IsString()
  public info: string;

  @ApiProperty({ example: 'Защищает от пыли, запахов и бактерий.' })
  @IsNotEmpty()
  @IsString()
  public downText: string;
}
