import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VideoReview {
  @IsNotEmpty()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'string' })
  public headline: string;

  @IsNotEmpty()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'string' })
  public description: string;

  @IsNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  @ApiProperty({ example: 'string' })
  public theses: string[];

  @IsNotEmpty()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'string' })
  public videoLink: string;
}
