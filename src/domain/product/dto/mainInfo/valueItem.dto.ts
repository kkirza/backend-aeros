import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ValueItem {
  @ApiProperty({ example: 'string' })
  @IsNotEmpty()
  @IsString()
  public value: string;

  @ApiProperty({ example: 'string' })
  @IsNotEmpty()
  @IsString()
  public subtitle: string;
}
