import { ValueItem } from '@/domain/product/dto/mainInfo/valueItem.dto';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LineItem {
  @IsNotEmpty()
  @ApiProperty({ type: ValueItem })
  public firstValue: ValueItem;

  @IsNotEmpty()
  @ApiProperty({ type: ValueItem })
  public secondValue: ValueItem;
}
