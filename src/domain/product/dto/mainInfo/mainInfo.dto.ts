import { LineItem } from '@/domain/product/dto/mainInfo/lineItem.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class MainInfo {
  @ApiProperty({ type: LineItem })
  @IsNotEmpty()
  public topLine: LineItem;

  @ApiProperty({ type: LineItem })
  @IsNotEmpty()
  public bottomLine: LineItem;
}
