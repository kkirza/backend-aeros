import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from '@/domain/category/category.entity';
import { Upload } from '@/domain/upload/upload.entity';
import { CategoryController } from '@/application/backoffice/category/category.controller';
import { CategoryService } from '@/services/category/category.service';

@Module({
  imports: [TypeOrmModule.forFeature([Category, Upload])],
  exports: [TypeOrmModule, CategoryService],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule {}
