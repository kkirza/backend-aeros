import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Upload } from '@/domain/upload/upload.entity';

@Entity('category')
export class Category extends BaseEntity {
  @ApiProperty({ example: 'Бризер', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false, length: 255 })
  public name: string;

  @ApiProperty({ example: 'Вытяжные', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false, length: 255 })
  public title: string;

  @ApiProperty({ example: 1, description: 'Тип категории для вывода' })
  @Column('int8', { nullable: false, default: 1 })
  public type: number;

  @ApiProperty({ type: Upload })
  @OneToOne(() => Upload, { nullable: true })
  @JoinColumn([{ name: 'image_id', referencedColumnName: 'id' }])
  public image?: Upload | null = null;

  @ApiProperty({ example: true })
  @Column('boolean', { nullable: false, default: true })
  public status: boolean = true;
}
