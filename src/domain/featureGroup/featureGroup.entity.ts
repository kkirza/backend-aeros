import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('featureGroup')
export class FeatureGroup extends BaseEntity {
  @ApiProperty({
    example: 'Способ хранения',
    description: 'Пустым быть не может',
  })
  @Column('varchar', { nullable: false })
  public title: string;

  @ApiProperty({ example: 1, description: 'Сортировка на вывод на фронте' })
  @Column('int8', { nullable: false, default: 0 })
  public sort: number;
}
