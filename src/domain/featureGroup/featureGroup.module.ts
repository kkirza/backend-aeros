import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeatureGroup } from '@/domain/featureGroup/featureGroup.entity';

@Module({
  imports: [TypeOrmModule.forFeature([FeatureGroup])],
  exports: [TypeOrmModule],
})
export class FeatureGroupModule {}
