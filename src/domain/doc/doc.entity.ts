import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Upload } from '@/domain/upload/upload.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity('doc')
export class Doc extends BaseEntity {
  @ManyToOne(() => Upload, undefined, { eager: true })
  @JoinColumn({ name: 'upload_id', referencedColumnName: 'id' })
  public upload: Upload;

  @ApiProperty({ example: 'документ' })
  @Column('varchar', { default: 'документ', nullable: false })
  public name: string = 'документ';

  @ManyToOne(() => ProductDocs, undefined, { eager: true })
  @JoinColumn({ name: 'product_doc_id', referencedColumnName: 'id' })
  public productDoc: ProductDocs;
}
