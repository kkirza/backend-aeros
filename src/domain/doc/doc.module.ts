import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Doc } from '@/domain/doc/doc.entity';
import { Upload } from '@/domain/upload/upload.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Doc, Upload, ProductDocs])],
  exports: [TypeOrmModule],
})
export class DocModule {}
