import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('upload')
export class Upload extends BaseEntity {
  @ApiProperty({
    example: '/test.webp',
    description: 'Пустым быть не может. Путь к файлу.',
  })
  @Column('varchar', { nullable: false })
  public filepath: string;
}
