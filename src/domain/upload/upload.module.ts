import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Upload } from '@/domain/upload/upload.entity';
import { FileService } from '@/infrastructure/fileSystem/file.service';
import { UploadController } from '@/application/backoffice/upload/upload.controller';
import { UploadService } from '@/services/upload/upload.service';

@Module({
  imports: [TypeOrmModule.forFeature([Upload])],
  exports: [TypeOrmModule, UploadService],
  providers: [FileService, UploadService],
  controllers: [UploadController],
})
export class UploadModule {}
