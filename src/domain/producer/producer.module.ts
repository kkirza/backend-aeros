import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Producer } from '@/domain/producer/producer.entity';
import { ProducerController } from '@/application/backoffice/producer/producer.controller';
import { ProducerService } from '@/services/producer/producer.service';

@Module({
  imports: [TypeOrmModule.forFeature([Producer])],
  exports: [TypeOrmModule, ProducerService],
  providers: [ProducerService],
  controllers: [ProducerController],
})
export class ProducerModule {}
