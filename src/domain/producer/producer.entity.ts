import { BaseEntity } from '@/infrastructure/database/entity/baseEntity';
import { Column, Entity } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('producer')
export class Producer extends BaseEntity {
  @ApiProperty({ example: 'Tion', description: 'Пустым быть не может' })
  @Column('varchar', { nullable: false })
  public name: string;

  @ApiProperty({ example: true })
  @Column('boolean', { nullable: false, default: true })
  public status: boolean = true;
}
