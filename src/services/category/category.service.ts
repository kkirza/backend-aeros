import { HttpException, Injectable } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Category } from '@/domain/category/category.entity';
import { BaseService } from '@/infrastructure/baseService/base.service';
import { CreateCategoryDto } from '@/application/backoffice/category/dto/createCategory/createCategory.dto';
import { Upload } from '@/domain/upload/upload.entity';

@Injectable()
export class CategoryService extends BaseService<Category> {
  constructor(
    @InjectDataSource()
    protected readonly dataSource: DataSource,
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {
    super(dataSource, () => Category);
  }

  public async create(dto: CreateCategoryDto): Promise<Category> {
    const image: Upload | null =
      await this.dataSource.manager.findOneBy<Upload>(Upload, {
        id: dto.imageId,
      });

    if (!image) {
      throw new HttpException('not found image by id: ' + dto.imageId, 404);
    }

    const category = this.categoryRepository.create({
      ...dto,
      image,
    });

    return await this.categoryRepository.save(category);
  }
}
