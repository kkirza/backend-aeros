import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { Upload } from '@/domain/upload/upload.entity';
import { DataSource, DeleteResult, QueryRunner, Repository } from 'typeorm';
import { FileService } from '@/infrastructure/fileSystem/file.service';
import { HttpException, Injectable } from '@nestjs/common';
import { FileItem } from '@/infrastructure/fileSystem/fileItem';
import { BaseService } from '@/infrastructure/baseService/base.service';

@Injectable()
export class UploadService extends BaseService<Upload> {
  public constructor(
    @InjectDataSource()
    protected readonly dataSource: DataSource,
    private readonly fileService: FileService,
    @InjectRepository(Upload)
    private readonly uploadRepository: Repository<Upload>,
  ) {
    super(dataSource, () => Upload);
  }

  public async create(
    dto: Express.Multer.File,
    queryRunner: QueryRunner | null = null,
  ): Promise<Upload> {
    const imagePath: string = await this.fileService.createFile(dto);
    const upload: Upload = this.uploadRepository.create({
      filepath: imagePath,
    });
    if (queryRunner) {
      return await queryRunner.manager.save(Upload, upload);
    }
    return await this.uploadRepository.save<Upload>(upload);
  }

  public async deleteOneById(id: string): Promise<void> {
    const upload: Upload | null = await this.uploadRepository.findOneBy({ id });
    if (!upload) {
      throw new HttpException('upload not found by id: ' + id, 404);
    }

    const queryRunner: QueryRunner = this.dataSource.createQueryRunner();
    await queryRunner.startTransaction();
    try {
      const res: DeleteResult = await this.uploadRepository.delete({
        id: upload.id,
      });
      const filepath = upload.filepath;
      if (!res?.affected) {
        throw new Error('Ошибка удаления сущности');
      }
      await this.fileService.DeleteFileByPath(filepath);
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new HttpException(
        'Непредвиденная ошибка сервера: ' + err?.message || '',
        500,
      );
    } finally {
      await queryRunner.release();
    }
  }
}
