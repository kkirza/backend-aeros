import { HttpException, Injectable } from '@nestjs/common';
import { DataSource, In, Repository } from 'typeorm';
import { Product } from '@/domain/product/product.entity';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { CreateProductRequest } from '@/application/backoffice/product/dto/createRequest/createProductRequest';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { Upload } from '@/domain/upload/upload.entity';
import { Doc } from '@/domain/doc/doc.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';

@Injectable()
export class ProductService {
  public constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(ImageProductPivot)
    private readonly imageProductPivotRepository: Repository<ImageProductPivot>,
    @InjectRepository(Upload)
    private readonly uploadRepository: Repository<Upload>,
    @InjectRepository(Doc)
    private readonly docRepository: Repository<Doc>,
    @InjectRepository(ProductDocs)
    private readonly productDocsRepository: Repository<ProductDocs>,
    @InjectDataSource()
    private readonly dataSource: DataSource,
  ) {}

  public async create(dto: CreateProductRequest): Promise<Product> {
    let product: Product = this.productRepository.create(dto);
    const imagePivots: ImageProductPivot[] = [];
    const docs: Doc[] = [];
    let productDocs: ProductDocs | null = null;

    if (dto.imagePivotsDto?.length) {
      for (const imagePivot of dto.imagePivotsDto) {
        const image: Upload | null = await this.uploadRepository.findOneBy({
          id: imagePivot.imageId,
        });
        if (image) {
          imagePivots.push(
            this.imageProductPivotRepository.create({
              image: image,
              sort: imagePivot.sort,
              product: product,
            }),
          );
        }
      }
    }

    if (dto?.productDocsDto?.uploadIds?.length) {
      const uploads: Upload[] = await this.uploadRepository.findBy({
        id: In(dto.productDocsDto.uploadIds),
      });
      productDocs = this.productDocsRepository.create({
        description: dto.productDocsDto.description,
        product: Product,
      });

      uploads?.forEach((upload: Upload) => {
        productDocs &&
          docs.push(
            this.docRepository.create({
              upload: upload,
              productDoc: productDocs,
            }),
          );
      });
    }

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.startTransaction();
    try {
      product = await queryRunner.manager.save(product);

      if (productDocs) {
        productDocs.product = product;
        productDocs = await queryRunner.manager.save(productDocs);
        if (docs.length) {
          for (const doc of docs) {
            doc.productDoc = productDocs;
          }
          await queryRunner.manager.save(docs);
        }
      }

      if (imagePivots.length) {
        for (const imagePivot of imagePivots) {
          imagePivot.product = product;
        }
        await queryRunner.manager.save(imagePivots);
      }
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }

    return product;
  }

  public async findById(id: string): Promise<Product> {
    const product: Product | null = await this.productRepository.findOne({
      where: {
        id,
      },
      relations: { productDoc: true },
    });

    if (!product) {
      throw new HttpException('product not found', 404);
    }

    return product;
  }

  public async findAll(): Promise<Product[]> {
    return await this.productRepository.find({
      relations: {
        productDoc: true,
      },
    });
  }
}
