import { BaseService } from '@/infrastructure/baseService/base.service';
import { Producer } from '@/domain/producer/producer.entity';
import { DataSource, Repository } from 'typeorm';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { CreateProducerDto } from '@/application/backoffice/producer/dto/createProducer/createProducer.dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ProducerService extends BaseService<Producer> {
  public constructor(
    @InjectDataSource()
    protected readonly dataSource: DataSource,
    @InjectRepository(Producer)
    private readonly producerRepository: Repository<Producer>,
  ) {
    super(dataSource, () => Producer);
  }

  public async create(dto: CreateProducerDto): Promise<Producer> {
    const producer = this.producerRepository.create(dto);

    return await this.producerRepository.save(producer);
  }
}
