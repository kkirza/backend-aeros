import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';
import { UserModule } from '@/domain/user/user.module';
import { CategoryModule } from '@/domain/category/category.module';
import { DocModule } from '@/domain/doc/doc.module';
import { FeatureModule } from '@/domain/feature/feature.module';
import { FeatureGroupModule } from '@/domain/featureGroup/featureGroup.module';
import { FeatureProductModule } from '@/domain/featureProduct/featureProduct.module';
import { ImageProductPivotModule } from '@/domain/imageProductPivot/imageProductPivot.module';
import { ProducerModule } from '@/domain/producer/producer.module';
import { ProductModule } from '@/domain/product/product.module';
import { ProductDocsModule } from '@/domain/productDocs/productDocs.module';
import { UploadModule } from '@/domain/upload/upload.module';
import { CreateProductCommand } from '@/application/commands/product/createProduct/createProduct.command';
import { CommandModule } from 'nestjs-command';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USERNAME'),
        password: configService.get<string>('DB_PASSWORD'),
        database: configService.get<string>('DB_NAME'),
        synchronize: false,
        autoLoadEntities: true,
        entities: [__dirname + '/**/*.entity{.js, .ts}'],
        migrationsTableName: 'migrations',
        migrationsRun: true,
        migrationsTransactionMode: 'each',
        migrations: [__dirname + '../**/migrations/*.{.js, .ts}'],
      }),
      inject: [ConfigService],
    }),
    ServeStaticModule.forRoot({
      rootPath: path.resolve(__dirname, '..', '..', 'static'),
    }),
    UserModule,
    CategoryModule,
    DocModule,
    FeatureModule,
    FeatureGroupModule,
    FeatureProductModule,
    ImageProductPivotModule,
    ProducerModule,
    ProductModule,
    ProductDocsModule,
    UploadModule,
    CommandModule,
  ],
  controllers: [AppController],
  providers: [AppService, CreateProductCommand],
})
export class AppModule {}
