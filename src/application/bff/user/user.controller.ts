import { ApiTags } from '@nestjs/swagger';
import { Controller, Get } from '@nestjs/common';

@ApiTags('Пользователи')
@Controller('bff/api/v1/users')
export class UsersController {
  @Get()
  getHello(): string {
    return 'test';
  }
}
