import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, Param, Post, UsePipes } from '@nestjs/common';
import { Product } from '@/domain/product/product.entity';
import { CreateProductRequest } from '@/application/backoffice/product/dto/createRequest/createProductRequest';
import { ProductService } from '@/services/product/product.service';
import { ValidationPipe } from '@/infrastructure/pipes/validation/validation.pipe';

@ApiTags('Товары')
@Controller('backoffice/api/v1/product')
export class ProductBackofficeController {
  public constructor(private readonly productService: ProductService) {}

  @Get()
  @ApiResponse({ status: 200, type: [Product] })
  public async getAll(): Promise<Product[]> {
    return await this.productService.findAll();
  }

  @Post()
  @UsePipes(ValidationPipe)
  @ApiResponse({ status: 201, type: [Product] })
  public async create(@Body() dto: CreateProductRequest): Promise<Product> {
    return await this.productService.create(dto);
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Product> {
    return this.productService.findById(id);
  }
}
