import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class ProductDocsRequest {
  @ApiProperty({ example: 'Описание документации' })
  @IsNotEmpty()
  @IsString()
  public description: string;

  @IsUUID('4', { each: true })
  @ApiProperty({ example: ['550e8400-e29b-41d4-a716-446655440000'] })
  public uploadIds: string[];
}
