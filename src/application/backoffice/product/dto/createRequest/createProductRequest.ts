import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  Min,
  ValidateNested,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ImagePivotRequest } from '@/application/backoffice/product/dto/createRequest/imagePivotRequest';
import { Type } from 'class-transformer';
import { VideoReviewRequest } from '@/application/backoffice/product/dto/createRequest/videoReviewRequest';
import { ProductDocsRequest } from '@/application/backoffice/product/dto/createRequest/productDocsRequest';

export class CreateProductRequest {
  @IsNotEmpty()
  @ApiProperty({ example: [ImagePivotRequest] })
  @Type(() => ImagePivotRequest)
  @ValidateNested({ each: true })
  public imagePivotsDto: ImagePivotRequest[] = [];

  @IsOptional()
  @Type(() => VideoReviewRequest)
  @ValidateNested()
  public videoReviewDto?: VideoReviewRequest;

  @IsOptional()
  @Type(() => ProductDocsRequest)
  @ValidateNested()
  public productDocsDto?: ProductDocsRequest;

  @IsNotEmpty({ message: 'Не может быть пустым' })
  @ApiProperty({ example: '550e8400-e29b-41d4-a716-446655440000' })
  @IsUUID()
  public categoryId: string;

  @IsNotEmpty({ message: 'Не может быть пустым' })
  @ApiProperty({ example: '550e8400-e29b-41d4-a716-446655440000' })
  @IsUUID()
  public producerId: string;

  @ApiProperty({ example: 'Tios 4S' })
  @IsString({ message: 'Поле должно быть строкой' })
  @IsNotEmpty({ message: 'Не может быть пустым' })
  public name: string;

  // @ApiProperty({ example: 'Описание товара' })
  @IsString({ message: 'Поле должно быть строкой' })
  @IsNotEmpty({ message: 'Не может быть пустым' })
  public description: string;

  @ApiProperty({ example: ['Приемущество', 'воздух'] })
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  public advantages?: string[] = [];

  @ApiProperty({ example: 1000000 })
  @IsNotEmpty({ message: 'Не может быть пустым' })
  @IsNumber()
  @Min(1)
  public price: number;

  @ApiProperty({ example: ['мощный', 'до 30м'] })
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  public tags?: string[] = [];
}
