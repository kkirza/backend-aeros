import { IsNotEmpty, IsNumber, IsOptional, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ImagePivotRequest {
  @IsUUID('4')
  @IsNotEmpty()
  @ApiProperty({ example: '550e8400-e29b-41d4-a716-446655440000' })
  public imageId: string;

  @IsOptional()
  @IsNumber()
  public sort?: number = 0;
}
