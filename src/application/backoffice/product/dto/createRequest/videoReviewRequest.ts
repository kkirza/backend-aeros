import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VideoReviewRequest {
  @IsNotEmpty()
  @ApiProperty({ example: 'www.youtube.ru' })
  @IsString()
  public link: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Описание видео' })
  public description: string;

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  @ApiProperty({ example: ['Тезис 1', 'Тезис 2'] })
  public theses: string[];
}
