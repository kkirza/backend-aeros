import { IsNotEmpty, IsNumber, IsString, IsUUID, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FeatureProductRequest {
  @IsUUID('4')
  @IsNotEmpty()
  @ApiProperty({ example: '550e8400-e29b-41d4-a716-446655440000' })
  public featureId: string;

  @ApiProperty({ example: ['test', '2'] })
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  public values: string[];

  @ApiProperty({ example: 0 })
  @IsNumber()
  @Min(0)
  public sort: number = 0;
}
