import { ApiNoContentResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Upload } from '@/domain/upload/upload.entity';
import { UploadService } from '@/services/upload/upload.service';
import { FileItem } from '@/infrastructure/fileSystem/fileItem';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('Файлы')
@Controller('backoffice/api/v1/upload')
export class UploadController {
  public constructor(private readonly uploadService: UploadService) {}
  @Get(':id')
  @ApiResponse({ status: 200, type: Upload })
  public async findOne(@Param('id') id: string): Promise<Upload> {
    return await this.uploadService.findOneById(id);
  }

  @Get()
  @ApiResponse({ status: 200, type: [Upload] })
  public async findAll(): Promise<Upload[]> {
    return await this.uploadService.findAll<Upload>();
  }

  @Delete(':id')
  @HttpCode(204)
  @ApiNoContentResponse({ status: 204 })
  public async deleteOneById(@Param('id') id: string): Promise<void> {
    await this.uploadService.deleteOneById(id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({ status: HttpStatus.CREATED, type: Upload })
  @UseInterceptors(FileInterceptor('file'))
  public async create(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Upload> {
    return await this.uploadService.create(file);
  }
}
