import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CategoryService } from '@/services/category/category.service';
import { Category } from '@/domain/category/category.entity';
import { CreateCategoryDto } from '@/application/backoffice/category/dto/createCategory/createCategory.dto';

@ApiTags('Категории')
@Controller('backoffice/api/v1/category')
export class CategoryController {
  public constructor(private readonly categoryService: CategoryService) {}

  @Get()
  @ApiResponse({ status: 200, type: [Category] })
  public async findAll(): Promise<Category[]> {
    return await this.categoryService.findAll();
  }

  @Get(':id')
  @ApiResponse({ status: 200, type: Category })
  public async findOneById(@Param('id') id: string): Promise<Category> {
    return await this.categoryService.findOneById(id);
  }

  @Post()
  @ApiResponse({ status: 201, type: Category })
  public async create(@Body() dto: CreateCategoryDto): Promise<Category> {
    return await this.categoryService.create(dto);
  }
}
