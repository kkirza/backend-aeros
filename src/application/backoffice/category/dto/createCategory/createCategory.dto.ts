import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';

export class CreateCategoryDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'Бризер', description: 'Пустым быть не может' })
  public name: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ example: 1, description: 'Тип категории для вывода' })
  public type: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'описание' })
  public title: string;

  @IsNotEmpty()
  @IsUUID('4')
  @ApiProperty({ example: '550e8400-e29b-41d4-a716-446655440000' })
  public imageId: string;
}
