import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateProducerDto {
  @ApiProperty({ example: 'Tion' })
  @IsNotEmpty()
  @IsString()
  public name: string;
}
