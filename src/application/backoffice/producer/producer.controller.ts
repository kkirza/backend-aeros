import { ProducerService } from '@/services/producer/producer.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UsePipes,
} from '@nestjs/common';
import { ApiNoContentResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Producer } from '@/domain/producer/producer.entity';
import { CreateProducerDto } from '@/application/backoffice/producer/dto/createProducer/createProducer.dto';
import { ValidationPipe } from '@/infrastructure/pipes/validation/validation.pipe';

@ApiTags('Производители')
@Controller('backoffice/api/v1/producer')
export class ProducerController {
  public constructor(private readonly producerService: ProducerService) {}

  @Get()
  @ApiResponse({ status: 200, type: [Producer] })
  public async findAll(): Promise<Producer[]> {
    return await this.producerService.findAll();
  }

  @Get(':id')
  @ApiResponse({ status: 200, type: Producer })
  public async findOneById(@Param('id') id: string): Promise<Producer> {
    return await this.producerService.findOneById(id);
  }

  @Delete(':id')
  @ApiNoContentResponse({ status: 204 })
  @HttpCode(HttpStatus.NO_CONTENT)
  public async deleteById(@Param('id') id: string): Promise<void> {
    await this.producerService.deleteOneById(id);
  }

  @Post()
  @ApiResponse({ status: 201, type: Producer })
  @UsePipes(ValidationPipe)
  public async create(@Body() dto: CreateProducerDto): Promise<Producer> {
    return await this.producerService.create(dto);
  }
}
