import { SliderImageDto } from '@/application/commands/product/createProduct/dto/sliderImage.dto';
import { Service } from '@/domain/product/dto/service.dto';
import { InfoBlock } from '@/domain/product/dto/infoBlock.dto';
import { MainInfo } from '@/domain/product/dto/mainInfo/mainInfo.dto';
import { FeatureBlock } from '@/domain/product/dto/featureBlock.dto';
import { CharasteristicDto } from '@/application/commands/product/createProduct/dto/Charasteristic.dto';
import { VideoReview } from '@/domain/product/dto/videoReview.dto';
import { DocumentationDto } from '@/application/commands/product/createProduct/dto/Documentation.dto';
import { MontageDto } from '@/application/commands/product/createProduct/dto/montage.dto';

export class CreateProductDto {
  public sliderImages: SliderImageDto[] = [];
  public producer: string;
  public key: string;
  public productName: string;
  public price: number;
  public productCategory: string;
  public productTags: string[];
  public services: Service[] = [];
  public description: string;
  public mainImg: string;
  public advantages: string[] = [];
  public mainProperties: string[] = [];
  public downImg: string;
  public infoBlock?: InfoBlock;
  public mainInfo?: MainInfo;
  public featuresBlock?: FeatureBlock[];
  public characteristic?: CharasteristicDto[] = [];
  public videoReview?: VideoReview;
  public documentation?: DocumentationDto;
  public montage?: MontageDto;
}
