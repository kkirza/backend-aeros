import { CharasteristicItemDto } from '@/application/commands/product/createProduct/dto/CharasteristicItem.dto';

export class CharasteristicDto {
  public sectionName: string;
  public data: CharasteristicItemDto[] = [];
  public sort?: number | null = null;
}
