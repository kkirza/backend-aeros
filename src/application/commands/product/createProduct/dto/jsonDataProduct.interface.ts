import { SliderImageDto } from '@/application/commands/product/createProduct/dto/sliderImage.dto';
import { Service } from '@/domain/product/dto/service.dto';
import { InfoBlock } from '@/domain/product/dto/infoBlock.dto';
import { MainInfo } from '@/domain/product/dto/mainInfo/mainInfo.dto';
import { FeatureBlock } from '@/domain/product/dto/featureBlock.dto';
import { CharasteristicDto } from '@/application/commands/product/createProduct/dto/Charasteristic.dto';
import { VideoReview } from '@/domain/product/dto/videoReview.dto';
import { DocumentationDto } from '@/application/commands/product/createProduct/dto/Documentation.dto';
import { MontageDto } from '@/application/commands/product/createProduct/dto/montage.dto';

export interface JsonDataProductInterface {
  sliderImages: SliderImageDto[];
  producer: string;
  key: string;
  productName: string;
  price: number;
  productCategory: string;
  productTags: string[];
  services: Service[];
  aboutProduct: AboutProductInterface;
  characteristic?: CharasteristicDto[];
  videoReview?: VideoReview;
  documentation?: DocumentationDto;
  montage?: MontageDto;
}

interface AboutProductInterface {
  description: string;
  mainImg: string;
  advantages?: string[];
  downImg: string;
  infoBlock?: InfoBlock;
  mainInfo?: MainInfo;
  mainProperties?: string[];
  featuresBlock?: FeatureBlock[];
}
