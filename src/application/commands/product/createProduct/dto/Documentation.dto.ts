import { FileItemDto } from '@/application/commands/product/createProduct/dto/FileItem.dto';

export class DocumentationDto {
  public description: string;
  public files: FileItemDto[];
}
