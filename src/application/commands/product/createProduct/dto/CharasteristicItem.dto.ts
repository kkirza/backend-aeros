export class CharasteristicItemDto {
  public name: string;
  public value: string;
  public sort?: number | null = null;
}
