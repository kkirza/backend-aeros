export class SliderImageDto {
  public link: string;
  public sort: number = 0;
  public montagePhoto?: number | null;

  public constructor(
    link: string,
    sort?: number,
    montagePhoto?: number | null,
  ) {
    this.link = link;
    if (sort) {
      this.sort = sort;
    }
    this.montagePhoto = montagePhoto;
  }
}
