import { Injectable } from '@nestjs/common';
import { CommandInterface } from '@/infrastructure/cli/command.interface';
import { DataSource, QueryRunner } from 'typeorm';
import { InjectDataSource } from '@nestjs/typeorm';
import * as fs from 'fs';
import { CreateProductDto } from '@/application/commands/product/createProduct/dto/CreateProduct.dto';
import { SliderImageDto } from '@/application/commands/product/createProduct/dto/sliderImage.dto';
import { Service } from '@/domain/product/dto/service.dto';
import { JsonDataProductInterface } from '@/application/commands/product/createProduct/dto/jsonDataProduct.interface';
import { InfoBlock } from '@/domain/product/dto/infoBlock.dto';
import { MainInfo } from '@/domain/product/dto/mainInfo/mainInfo.dto';
import { LineItem } from '@/domain/product/dto/mainInfo/lineItem.dto';
import { ValueItem } from '@/domain/product/dto/mainInfo/valueItem.dto';
import { FeatureBlock } from '@/domain/product/dto/featureBlock.dto';
import { CharasteristicDto } from '@/application/commands/product/createProduct/dto/Charasteristic.dto';
import { CharasteristicItemDto } from '@/application/commands/product/createProduct/dto/CharasteristicItem.dto';
import { VideoReview } from '@/domain/product/dto/videoReview.dto';
import { MontageDto } from '@/application/commands/product/createProduct/dto/montage.dto';
import { DocumentationDto } from '@/application/commands/product/createProduct/dto/Documentation.dto';
import { FileItemDto } from '@/application/commands/product/createProduct/dto/FileItem.dto';
import { Command } from 'nestjs-command';
import { Product } from '@/domain/product/product.entity';
import { Category } from '@/domain/category/category.entity';
import { ImageProductPivot } from '@/domain/imageProductPivot/imageProductPivot.entity';
import { Upload } from '@/domain/upload/upload.entity';
import axios from 'axios';
import * as path from 'path';
import * as transliteration from 'transliteration';
import { UploadService } from '@/services/upload/upload.service';
import { Doc } from '@/domain/doc/doc.entity';
import { ProductDocs } from '@/domain/productDocs/productDocs.entity';
import { Producer } from '@/domain/producer/producer.entity';
import { FeatureGroup } from '@/domain/featureGroup/featureGroup.entity';
import { Feature } from '@/domain/feature/feature.entity';
import { FeatureProduct } from '@/domain/featureProduct/featureProduct.entity';

@Injectable()
export class CreateProductCommand implements CommandInterface {
  public constructor(
    @InjectDataSource()
    protected readonly dataSource: DataSource,
    protected readonly uploadService: UploadService,
  ) {}

  @Command({
    command: 'import:product',
    describe: 'import product from json',
  })
  public async execute(): Promise<void> {
    const data = fs.readFileSync('./products.json', 'utf8');
    const list = this.getListFromString(data);

    for (const item of list) {
      const queryRunner = this.dataSource.createQueryRunner();
      await queryRunner.startTransaction();
      try {
        await this.createProductFromDto(item, queryRunner);
        await queryRunner.commitTransaction();
      } catch (err) {
        await queryRunner.rollbackTransaction();
        throw err;
      } finally {
        await queryRunner.release();
      }
    }
  }

  private async createProductFromDto(
    dto: CreateProductDto,
    queryRunner: QueryRunner,
  ): Promise<void> {
    const product = new Product();
    product.category = await this.findOrCreateCategoryByName(
      dto.productCategory,
      queryRunner,
    );
    product.producer = await this.findOrCreateProducerByName(
      dto.producer,
      queryRunner,
    );
    product.mainImage = await this.createUploadFromUrl(
      dto.mainImg,
      queryRunner,
    );
    product.downImage = await this.createUploadFromUrl(
      dto.downImg,
      queryRunner,
    );
    product.videoReview = dto.videoReview;
    product.key = dto.key;
    product.advantages = dto.advantages;
    product.description = dto.description;
    product.buyWithProducts = [];
    product.infoBlock = dto.infoBlock;
    product.montageType =
      dto.montage?.category === 'Бризер'
        ? Product.MONTAGE_TYPE_BRIZER
        : dto.montage?.category === 'Кондиционер'
          ? Product.MONTAGE_TYPE_CONDITIONER
          : null;
    product.mainProperties = dto.mainProperties;
    product.tags = dto.productTags;
    product.infoBlock = dto.infoBlock;
    product.mainInfo = dto.mainInfo;
    product.services = dto.services;
    product.name = dto.productName;
    product.price = dto.price;
    product.featuresBlock = dto.featuresBlock;

    await queryRunner.manager.save(Product, product);
    if (dto.characteristic) {
      await this.createFeatures(product, dto.characteristic, queryRunner);
    }
    if (dto.documentation) {
      await this.createDocs(product, dto.documentation, queryRunner);
    }
    await this.createImages(product, dto.sliderImages, queryRunner);
  }

  private getListFromString(data: string): CreateProductDto[] {
    const list: JsonDataProductInterface[] = JSON.parse(data);
    const result: CreateProductDto[] = [];
    list.forEach((item: JsonDataProductInterface) => {
      try {
        result.push(this.createDtoFromJson(item));
      } catch (e) {
        console.error(e);
      }
    });

    return result;
  }

  private createDtoFromJson(item: JsonDataProductInterface): CreateProductDto {
    const dto = new CreateProductDto();
    const sliderImages: SliderImageDto[] = [];
    const services: Service[] = [];
    const featuresBlock: FeatureBlock[] = [];
    const characteristic: CharasteristicDto[] = [];

    item?.sliderImages?.forEach((slider) => {
      sliderImages.push(
        new SliderImageDto(
          slider.link,
          slider?.sort || 0,
          slider?.montagePhoto || null,
        ),
      );
    });

    item?.services?.forEach((service) => {
      const serviceDto = new Service();
      serviceDto.name = service.name;
      serviceDto.text = service.text || '';
      serviceDto.headline = service.headline;
      services.push(serviceDto);
    });

    item.aboutProduct.featuresBlock?.forEach((feature) => {
      const featureBlockDto = new FeatureBlock();
      featureBlockDto.text = feature.text;
      featureBlockDto.headline = feature.headline;
      featuresBlock.push(featureBlockDto);
    });

    item.characteristic?.forEach((char) => {
      const charasteristicDto = new CharasteristicDto();
      charasteristicDto.sort = char.sort || null;
      charasteristicDto.sectionName = char.sectionName;
      charasteristicDto.data = [];
      char.data?.forEach((dataItem) => {
        const charasteristicItemDto = new CharasteristicItemDto();
        charasteristicItemDto.name = dataItem.name;
        charasteristicItemDto.sort = dataItem.sort || null;
        charasteristicItemDto.value = dataItem.value;
        charasteristicDto.data.push(charasteristicItemDto);
      });

      characteristic.push(charasteristicDto);
    });

    dto.sliderImages = sliderImages;
    dto.services = services;
    dto.featuresBlock = featuresBlock;
    dto.characteristic = characteristic;
    dto.producer = item.producer;
    dto.key = item.key;
    dto.productName = item.productName;
    dto.price = item.price;
    dto.productCategory = item.productCategory;
    dto.productTags = item.productTags || [];
    dto.description = item.aboutProduct.description;
    dto.mainImg = item.aboutProduct.mainImg;
    dto.downImg = item.aboutProduct.downImg;
    dto.advantages = item.aboutProduct.advantages || [];
    dto.mainProperties = item.aboutProduct.mainProperties || [];
    if (item.aboutProduct?.infoBlock?.info) {
      const infoBlock = new InfoBlock();
      infoBlock.info = item.aboutProduct.infoBlock.info;
      infoBlock.headline = item.aboutProduct.infoBlock.headline;
      infoBlock.downText = item.aboutProduct.infoBlock.downText;
      dto.infoBlock = infoBlock;
    }
    if (item.aboutProduct?.mainInfo?.topLine) {
      const mainInfo = new MainInfo();
      const topLine = new LineItem();
      const topFirstValue = new ValueItem();
      const topSecondValue = new ValueItem();
      topFirstValue.value = item.aboutProduct.mainInfo.topLine.firstValue.value;
      topFirstValue.subtitle =
        item.aboutProduct.mainInfo.topLine.firstValue.subtitle;
      topLine.firstValue = topFirstValue;

      topSecondValue.value =
        item.aboutProduct.mainInfo.topLine.secondValue.value;
      topSecondValue.subtitle =
        item.aboutProduct.mainInfo.topLine.secondValue.subtitle;
      topLine.secondValue = topSecondValue;

      const bottomLine = new LineItem();
      const bottomFirstValue = new ValueItem();
      const bottomSecondValue = new ValueItem();

      bottomFirstValue.value =
        item.aboutProduct.mainInfo.bottomLine.firstValue.value;
      bottomFirstValue.subtitle =
        item.aboutProduct.mainInfo.bottomLine.firstValue.subtitle;
      bottomLine.firstValue = bottomFirstValue;

      bottomSecondValue.value =
        item.aboutProduct.mainInfo.bottomLine.secondValue.value;
      bottomSecondValue.subtitle =
        item.aboutProduct.mainInfo.bottomLine.secondValue.subtitle;
      bottomLine.secondValue = bottomSecondValue;

      mainInfo.topLine = topLine;
      mainInfo.bottomLine = bottomLine;
    }

    if (item?.videoReview?.videoLink) {
      const videoReview = new VideoReview();
      videoReview.description = item.videoReview.description;
      videoReview.headline = item.videoReview.headline;
      videoReview.theses = item.videoReview.theses;
      videoReview.videoLink = item.videoReview.videoLink;
      dto.videoReview = videoReview;
    }

    if (item?.documentation?.description) {
      const documentation = new DocumentationDto();
      documentation.description = item.documentation.description;
      documentation.files = [];
      item.documentation?.files?.forEach((file) => {
        const fileDto = new FileItemDto();
        fileDto.name = file.name;
        fileDto.link = file.link;
        documentation.files.push(fileDto);
      });
      dto.documentation = documentation;
    }

    if (item?.montage?.category) {
      const montage = new MontageDto();
      montage.category = item.montage.category;
      dto.montage = montage;
    }

    return dto;
  }

  private async findOrCreateCategoryByName(
    productCategory: string,
    queryRunner: QueryRunner,
  ): Promise<Category> {
    let category = await queryRunner.manager.findOne(Category, {
      where: { name: productCategory },
    });
    if (!category) {
      category = new Category();
      category.name = productCategory;
      category.title = productCategory;
      category = await queryRunner.manager.save(Category, category);
    }

    return category;
  }

  private async findOrCreateProducerByName(
    producerName: string,
    queryRunner: QueryRunner,
  ): Promise<Producer> {
    let producer = await queryRunner.manager.findOne(Producer, {
      where: { name: producerName },
    });
    if (!producer) {
      producer = new Producer();
      producer.name = producerName;
      producer = await queryRunner.manager.save(Producer, producer);
    }

    return producer;
  }

  private async findOrCreateFeatureGroupByName(
    queryRunner: QueryRunner,
    sectionName: string,
    sort?: number | null,
  ): Promise<FeatureGroup> {
    let featureGroup = await queryRunner.manager.findOne(FeatureGroup, {
      where: { title: sectionName },
    });
    if (!featureGroup) {
      featureGroup = new FeatureGroup();
      featureGroup.title = sectionName;
      featureGroup.sort = sort || 0;
      featureGroup = await queryRunner.manager.save(FeatureGroup, featureGroup);
    }

    return featureGroup;
  }

  private async findOrCreateFeatureByNameAndGroup(
    featureGroup: FeatureGroup,
    char: CharasteristicItemDto,
    queryRunner: QueryRunner,
  ): Promise<Feature> {
    let feature = await queryRunner.manager.findOne(Feature, {
      where: {
        title: char.name,
        group: {
          id: featureGroup.id,
        },
      },
    });
    if (!feature) {
      feature = new Feature();
      feature.title = char.name;
      feature.key = transliteration.transliterate(char.name, {
        replace: [[' ', '-']],
      });
      feature.sort = char.sort || 0;
      feature.group = featureGroup;
      feature = await queryRunner.manager.save(Feature, feature);
    }

    return feature;
  }

  private async createImages(
    product: Product,
    sliderImages: SliderImageDto[],
    queryRunner: QueryRunner,
  ): Promise<void> {
    const imageProductPivots = [];
    for (const slider of sliderImages) {
      const upload = await this.createUploadFromUrl(slider.link, queryRunner);
      const imageProductPivot = new ImageProductPivot();
      imageProductPivot.image = upload;
      imageProductPivot.montage = slider.montagePhoto || null;
      imageProductPivot.sort = slider.sort;
      imageProductPivot.product = product;
      imageProductPivots.push(imageProductPivot);
    }

    product.image = imageProductPivots;
    await queryRunner.manager.save(ImageProductPivot, imageProductPivots);
  }

  private async createUploadFromUrl(
    url: string,
    queryRunner: QueryRunner | null = null,
  ): Promise<Upload> {
    const response = await axios({
      method: 'get',
      url: url,
      responseType: 'arraybuffer',
    });
    const urlParsed = new URL(url);
    const filename = path.basename(urlParsed.pathname);
    return await this.uploadService.create(
      // @ts-ignore
      {
        originalname: filename,
        buffer: response.data,
      },
      queryRunner,
    );
  }

  private async createDocs(
    product: Product,
    documentation: DocumentationDto,
    queryRunner: QueryRunner,
  ): Promise<void> {
    const productDocs = await queryRunner.manager.save(
      this.dataSource.manager.create(ProductDocs, {
        product,
        description: documentation.description,
      }),
    );
    for (const file of documentation.files) {
      const doc = new Doc();
      doc.productDoc = productDocs;
      doc.upload = await this.createUploadFromUrl(file.link);
      doc.name = file.name || '';
      await queryRunner.manager.save(Doc, doc);
    }
  }

  private async createFeatures(
    product: Product,
    characteristics: CharasteristicDto[],
    queryRunner: QueryRunner,
  ): Promise<void> {
    for (const char of characteristics) {
      const featureGroup = await this.findOrCreateFeatureGroupByName(
        queryRunner,
        char.sectionName,
        char.sort,
      );
      for (const charItem of char.data) {
        const feature = await this.findOrCreateFeatureByNameAndGroup(
          featureGroup,
          charItem,
          queryRunner,
        );
        await queryRunner.manager.save(
          FeatureProduct,
          this.dataSource.manager.create(FeatureProduct, {
            product,
            feature,
            values: [charItem.value],
          }),
        );
      }
    }
  }
}
